# Continuous Delivery Pipelines with Spinnaker and Kubernetes Engine

## Objectives
Set up your environment by launching Google Cloud Shell, creating a Kubernetes Engine cluster, and configuring your identity and user management scheme.

Download a sample application, create a Git repository then upload it to a Google Cloud Source Repository.

Deploy Spinnaker to Kubernetes Engine using Helm.

Build your Docker image.

Create triggers to create Docker images when your application changes.

Configure a Spinnaker pipeline to reliably and continuously deploy your application to Kubernetes Engine.

Deploy a code change, triggering the pipeline, and watch it roll out to production.
## Pipeline architecture

![CI/CD](../resources/cicd.png)

### Application delivery pipeline

![Pipeline](../resources/pipeline.png)


## Setup and Requirements

```sh
gcloud auth list
gcloud config list project
```

## Set up your environment

1. Set the default compute zone
```sh
gcloud config set compute/zone us-central1-f
```

1. Create a Kubernetes Engine using the Spinnaker tutorial sample application
```sh
gcloud container clusters create spinnaker-tutorial \
    --machine-type=n1-standard-2
```

### Configure identity and access management

1. Create the service account: 
```sh
gcloud iam service-accounts create spinnaker-account \
    --display-name spinnaker-account
```

1. Store the service account email address and your current project ID in environment variables for use in later commands.
```sh
export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)')
export PROJECT=$(gcloud info --format='value(config.project)')
```

1. Bind the storage.admin role to your service account:
```sh
gcloud projects add-iam-policy-binding $PROJECT \
    --role roles/storage.admin \
    --member serviceAccount:$SA_EMAIL
```

1. Download the service account key. In a later step, you will install Spinnaker and upload this key to Kubernetes Engine
```sh
gcloud iam service-accounts keys create spinnaker-sa.json \
     --iam-account $SA_EMAIL
```

## Set up Cloud Pub/Sub to trigger Spinnaker pipelines

1. Create the Cloud Pub/Sub topic for notifications from Container Registry.
1. Create a subscription that Spinnaker can read from to receive notifications of images being pushed.
1. Give Spinnaker's service account permissions to read from the gcr-triggers subscription.

```sh
gcloud pubsub topics create projects/$PROJECT/topics/gcr
gcloud pubsub subscriptions create gcr-triggers \
    --topic projects/${PROJECT}/topics/gcr
export SA_EMAIL=$(gcloud iam service-accounts list \
    --filter="displayName:spinnaker-account" \
    --format='value(email)')
gcloud beta pubsub subscriptions add-iam-policy-binding gcr-triggers \
    --role roles/pubsub.subscriber --member serviceAccount:$SA_EMAIL
```

## Deploying Spinnaker using Helm

### Install Helm

```sh
wget https://get.helm.sh/helm-v3.1.0-linux-amd64.tar.gz
tar zxfv helm-v3.1.0-linux-amd64.tar.gz
cp linux-amd64/helm .
kubectl create clusterrolebinding user-admin-binding \
    --clusterrole=cluster-admin --user=$(gcloud config get-value account)
./helm repo add stable https://kubernetes-charts.storage.googleapis.com
./helm repo update
```

### Configure Spinnaker

```sh
export PROJECT=$(gcloud info \
    --format='value(config.project)')
export BUCKET=$PROJECT-spinnaker-config
gsutil mb -c regional -l us-central1 gs://$BUCKET
```

```sh
export SA_JSON=$(cat spinnaker-sa.json)
export PROJECT=$(gcloud info --format='value(config.project)')
export BUCKET=$PROJECT-spinnaker-config
cat > spinnaker-config.yaml <<EOF
gcs:
  enabled: true
  bucket: $BUCKET
  project: $PROJECT
  jsonKey: '$SA_JSON'

dockerRegistries:
- name: gcr
  address: https://gcr.io
  username: _json_key
  password: '$SA_JSON'
  email: 1234@5678.com

# Disable minio as the default storage backend
minio:
  enabled: false

# Configure Spinnaker to enable GCP services
halyard:
  additionalScripts:
    create: true
    data:
      enable_gcs_artifacts.sh: |-
        \$HAL_COMMAND config artifact gcs account add gcs-$PROJECT --json-path /opt/gcs/key.json
        \$HAL_COMMAND config artifact gcs enable
      enable_pubsub_triggers.sh: |-
        \$HAL_COMMAND config pubsub google enable
        \$HAL_COMMAND config pubsub google subscription add gcr-triggers \
          --subscription-name gcr-triggers \
          --json-path /opt/gcs/key.json \
          --project $PROJECT \
          --message-format GCR
EOF
```

### Deploy the Spinnaker chart

1. Use the Helm command-line interface to deploy the chart with your configuration set.
1. After the command completes, run the following command to set up port forwarding to Spinnaker from Cloud Shell.

```sh
./helm install -n default cd stable/spinnaker -f spinnaker-config.yaml \
           --version 1.23.0 --timeout 10m0s --wait
export DECK_POD=$(kubectl get pods --namespace default -l "cluster=spin-deck" \
    -o jsonpath="{.items[0].metadata.name}")
kubectl port-forward --namespace default $DECK_POD 8080:9000 >> /dev/null &
```

## Building the Docker Image

### Create your source code repository


```sh
wget https://gke-spinnaker.storage.googleapis.com/sample-app-v2.tgz
tar xzfv sample-app-v2.tgz
cd sample-app
git config --global user.email "$(gcloud config get-value core/account)"
git config --global user.name "[USERNAME]"
git init
git add .
git commit -m "Initial commit"
gcloud source repos create sample-app
git config credential.helper gcloud.sh
export PROJECT=$(gcloud info --format='value(config.project)')
git remote add origin https://source.developers.google.com/p/$PROJECT/r/sample-app
git push origin master
```

### Configure your build triggers

We will create a build trigger based on a tag to the repo.

### Prepare your Kubernetes Manifests for use in Spinnaker

```sh
export PROJECT=$(gcloud info --format='value(config.project)')
gsutil mb -l us-central1 gs://$PROJECT-kubernetes-manifests
gsutil versioning set on gs://$PROJECT-kubernetes-manifests
sed -i s/PROJECT/$PROJECT/g k8s/deployments/*
git commit -a -m "Set project ID"
```

### Build your image

```sh
git tag v1.0.0
git push --tags
```

## Configuring your deployment pipelines

### Install the spin CLI for managing Spinnaker

```sh
curl -LO https://storage.googleapis.com/spinnaker-artifacts/spin/1.14.0/linux/amd64/spin
chmod +x spin
```

### Create the deployment pipeline

```sh
./spin application save --application-name sample \
                        --owner-email "$(gcloud config get-value core/account)" \
                        --cloud-providers kubernetes \
                        --gate-endpoint http://localhost:8080/gate
export PROJECT=$(gcloud info --format='value(config.project)')
sed s/PROJECT/$PROJECT/g spinnaker/pipeline-deploy.json > pipeline.json
./spin pipeline save --gate-endpoint http://localhost:8080/gate -f pipeline.json
```

### Manually Trigger and View your pipeline execution


## Triggering your pipeline from code changes

```sh
sed -i 's/orange/blue/g' cmd/gke-info/common-service.go
git commit -a -m "Change color to blue"
git tag v1.0.1
git push --tags
```

## Observe the canary deployments

```sh
git revert v1.0.1
git tag v1.0.2
git push --tags
```

## Congratulations
