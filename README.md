# Cloud Architecture [Quest][1]

## Index

1. Orchestrating the Cloud with Kubernetes
1. [Deployment Manager - Full Production][2] (included in the cloud-engineering quest)
1. Continuous Delivery Pipelines with Spinnaker and Kubernetes Engine
1. [Multiple VPC Networks][3] (included in the cloud-engineering quest)
1. Site Reliability Troubleshooting with Cloud Monitoring APM
1. Cloud Architecture: Challenge Lab

[1]: https://google.qwiklabs.com/quests/24
[2]: https://gitlab.com/google-quests/cloud-engineering/-/tree/master/deployment-manager
[3]: https://gitlab.com/google-quests/cloud-engineering/-/tree/master/multiple-vpc-networks